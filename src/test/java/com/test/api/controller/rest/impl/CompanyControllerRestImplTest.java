package com.test.api.controller.rest.impl;

import com.test.api.controller.rest.json.CompanyResponse;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static net.bytebuddy.matcher.ElementMatchers.is;

@WebMvcTest(CompanyControllerRestImplTest.class)
@ExtendWith(MockitoExtension.class)
class CompanyControllerRestImplTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CompanyControllerRestImpl categoryControllerRest;

    @Test
    void getAllCompanies() throws Exception {
        String response = "Get all companies";
        CompanyResponse<String> expected = new CompanyResponse<>(String.valueOf(HttpStatus.OK.value()), HttpStatus.OK.toString(), response);
        String url = "/company";

        Mockito.when(categoryControllerRest.getAllCompanies()).thenReturn(expected);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andExpectAll(
                        MockMvcResultMatchers.status().isOk(),
                        MockMvcResultMatchers.content().contentType("application/json"),
                        MockMvcResultMatchers.jsonPath("$.data").value("Get all companies")
                );
    }
}