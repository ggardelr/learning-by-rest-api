package com.test.api.service.impl;

import com.test.api.service.CompanyService;
import org.springframework.stereotype.Service;

@Service
public class CompanyServiceImpl implements CompanyService {
    @Override
    public String getAllCompanies() {
        String response = "Get all companies";

        return response;
    }
}
