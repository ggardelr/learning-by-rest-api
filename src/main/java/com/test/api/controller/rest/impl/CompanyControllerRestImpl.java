package com.test.api.controller.rest.impl;

import com.test.api.controller.rest.CompanyControllerRest;
import com.test.api.controller.rest.json.CompanyResponse;
import com.test.api.service.CompanyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(value = "*")
@RequiredArgsConstructor
public class CompanyControllerRestImpl implements CompanyControllerRest {
    private final CompanyService companyService;

    @Override
    @GetMapping("/company")
    @ResponseStatus(HttpStatus.OK)
    public CompanyResponse<String> getAllCompanies() {
        return new CompanyResponse<>(String.valueOf(HttpStatus.OK.value()), HttpStatus.OK.toString(), companyService.getAllCompanies());
    }
}
