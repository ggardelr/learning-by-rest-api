package com.test.api.controller.rest.json;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class CompanyResponse<T> implements Serializable {
    private String status_code;

    private String status_message;

    private T data;
}
