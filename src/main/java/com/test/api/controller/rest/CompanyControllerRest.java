package com.test.api.controller.rest;

import com.test.api.controller.rest.json.CompanyResponse;

public interface CompanyControllerRest {
    CompanyResponse<String> getAllCompanies();
}
